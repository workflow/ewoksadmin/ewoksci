# Docker images for CI

## Update all docker images

```bash
sudo ./update_images.sh <username> <token> [<match-pattern>]
```

The token can be found on https://harbor.esrf.fr

## Build image

```bash
sudo docker build -t docker-registry.esrf.fr/dau/ewoks:python_3.7 -f images/python_3.7.asc ./images
```

## Test image

```bash
sudo docker images
sudo docker run -i -t docker-registry.esrf.fr/dau/ewoks:python_3.7 /bin/bash
```

## Upload image to Harbor

```bash
sudo docker login https://docker-registry.esrf.fr -u [username] -p [CLI id]
sudo docker push docker-registry.esrf.fr/dau/ewoks:python_3.7
```

## Run image manually

```bash
sudo docker run -it --ulimit core=-1 --init -v /users/${USER}:/output docker-registry.esrf.fr/dau/ewoks:python_3.7 /bin/bash
```

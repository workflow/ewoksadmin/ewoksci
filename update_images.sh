#!/usr/bin/env bash
#
#  sudo ./update_images.sh <username> <token>
#
#  The token can be found on https://harbor.esrf.fr


function main {

    docker login https://docker-registry.esrf.fr -u $1 -p $2
    local tag=""
    local filter="*.asc"
    if [[ $3 ]];then
        filter=$3
    fi

    for filename in ./images/$filter;do
        tag=$(basename -- "$filename")
        tag="${tag%.*}"
        echo ""
        echo "------$tag------"
        docker build --no-cache --tag=docker-registry.esrf.fr/dau/ewoks:$tag --file=$filename ./images
        docker push docker-registry.esrf.fr/dau/ewoks:$tag
        docker rmi docker-registry.esrf.fr/dau/ewoks:$tag
        docker system prune -a -f
    done
}

main $@
